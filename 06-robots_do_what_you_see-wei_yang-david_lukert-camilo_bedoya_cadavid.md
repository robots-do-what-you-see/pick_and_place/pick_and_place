# Robots do what you see
A Unity AR App with ROS Integration aiming to create an AR Interface for Human and Robot to ease the communication prolems between human and robot on construction site.

## Quick Setup
1. `git clone --recurse-submodules https://gitlab.com/robots-do-what-you-see/pick_and_place/ROS.git`
1. `cd ROS`
1. `./build_image.sh`
1. `./launch_planner.sh`
1. git clone https://gitlab.com/robots-do-what-you-see/pick_and_place/PickAndPlaceProject.git
1. Open PickAndPlaceProject with [Unity Hub](https://unity.com/)
1. Open Scenes/PickAndPlaceAR.unity and Play
1. If ROS/Docker is not running on the same machine, Click Settings and change _**`ROS IP/Port`**_
1. Toggle _**`Connect`**_ or _**`Connect On Start`**_ to connect to ROS TCP Server.
1. Click _**`Save & Close`**_ to close Settings Menu.
1. Click _**`Next`**_ to run the simulation, _**`Previous`**_ to put the brick back.
![](/media/setting_menu.webp)
1. Build for iOS (with X Code)/ Andorid (with Android SDK)
1. Click _**`Place Robot`**_ to place the robot to a proper place
1. Setting: _**`Table`**_ show/hide table

## Overview

1. Try [ARCore2][1] & [AREditor][2] Repo - Wei, David
1. Try [Unity Robotics Hub][3] Repo - Wei, David, Camilo
1. [Integrate with AR Feature][4] - Wei
1. [Integrate the work of group][8] - Wei, David, Camilo
1. Add [Approach & Retract Pose][9] in ROS Intergration - Wei
1. Try [Speckle-Unity][5] - Wei, David
1. Try [Manomotion SDK][6] - Wei
1. Try [ToF AR SDK][7] - Wei
1. Try [Azure Spatial Anchors][10] - Camilo

[1]: https://github.com/robofit/arcor2/
[2]: https://github.com/robofit/arcor2_areditor
[3]: https://github.com/Unity-Technologies/Unity-Robotics-Hub
[4]: https://www.slideshare.net/UnityTechnologiesJapan002/unity-robotics-lt20219
[5]: https://github.com/specklesystems/speckle-unity
[6]: https://www.manomotion.com/
[7]: https://developer.sony.com/develop/tof-ar
[8]: https://gitlab.com/robots-do-what-you-see/pick_and_place/pick_and_place
[9]: https://gitlab.com/robots-do-what-you-see/pick_and_place/ROS/-/blame/main/src/niryo_moveit/scripts/mover.py
[10]:https://docs.microsoft.com/en-us/azure/spatial-anchors/overview


## About ARCore2 Repo (ditched)
1. This repo currently only work on Linux x86_64. Linux arm64 is not supported due to dependency on the [test-it-off](https://gitlab.com/kinalisoft/test-it-off) private repo
1. Buiding docker images with [integration with pantsbuild](https://github.com/wolfboyyang/arcor2/commit/2e7cf0db4d46ba1df44b5e947b860636a6592bb5).
1. The Unity App [AREditor](https://github.com/robofit/arcor2_areditor), depends on two none-free package,[Modern UI Pack](https://assetstore.unity.com/packages/tools/gui/modern-ui-pack-201717) & [TriLib 2](https://assetstore.unity.com/packages/tools/modeling/trilib-2-model-loading-package-157548), and one outdated package [Simple Collada](https://assetstore.unity.com/packages/tools/input-management/simple-collada-19579), which make it hard to reuse this repo out of box.
1. Considered the complexity of this repo and found of Unity Robotics Hub, arcor2 is ditched and just for some referrence.
1. This repo gave us good understanding of the AR anchor system & Network Achitecture for Unity/ROS integration.

## [Unity Robotics Hub](https://github.com/Unity-Technologies/Unity-Robotics-Hub) - Wei, David, Camilo
![](/media/warehouse.gif)
![](/media/unity_robotics_hub_architecture.webp)
### [Installing the Unity Robotics packages](https://github.com/Unity-Technologies/Unity-Robotics-Hub/blob/main/tutorials/quick_setup.md)
1. Install [ROS-TCP-Connector](https://github.com/Unity-Technologies/ROS-TCP-Connector) with git-url: `https://github.com/Unity-Technologies/ROS-TCP-Connector.git?path=/com.unity.robotics.ros-tcp-connector`.
1. Install [URDF-Importer](https://github.com/Unity-Technologies/URDF-Importer) with git-url: `https://github.com/Unity-Technologies/URDF-Importer.git?path=/com.unity.robotics.urdf-importer`
1. For mobile(iOS/android), you need to move the URDF-Importer form Library/PackageCache/ to Packages/ to be able to toggle Platforms/iOS (and Android) for `Packages/com.unity.robotics.urdf-importer/Runtime/Unity.Robotics.URDFImporter.asmdef`
![](/media/uniy_urdf_importer.webp)

### [Pick-and-Place Tutorial Part 0-3](https://github.com/Unity-Technologies/Unity-Robotics-Hub/blob/main/tutorials/pick_and_place/README.md)

### Integrate with AR - Wei
1. [Youtube Video: ARとUnity-Robotics-Hubの連携](https://youtu.be/2CtzSHunKMo)
1. [Youtube Demo](https://youtu.be/h6jWV_IM7Pw)
1. [Slide](https://www.slideshare.net/UnityTechnologiesJapan002/unity-robotics-lt20219)

### Unity Network Access Issue
When I run the Unity App on iOS, there's no Wifi/Celler setting only LocalNetwork, which makes the connection to Unity-ROS-TCP server failed. I use the following scripts to fix this issue. You could add this to any Start() or Awake().
```csharp
// Check Internet Access
if(Application.internetReachability == NetworkReachability.NotReachable)
{
  Debug.Log("Error. Check internet connection!");
}
```

### Integrate Work from David and Camilo - Wei
[Gitlab Repo](https://gitlab.com/robots-do-what-you-see/pick_and_place/pick_and_place)
1. PickAndPlaneProject submodule for Unity Project
1. ROS submodule for ROS Intergration
1. Setup Linux Server for Test. Use [Zellij](https://zellij.dev/) for detached session.

### TestFlight Build for iOS devices
1. RWTH Aachen University is registered as Apple Developer Team, but you need to contact IT Desk ([Bernd Decker](https://www.itc.rwth-aachen.de/cms/IT-Center/IT-Center/Team/~epvp/Mitarbeiter-CAMPUS-/?gguid=0x5FC1F49F6FD40940AD775431A62BB228)) to add your apple id to this team.
1. For Intenal Test, all tester needs to be in the team. When the process is completed, you need to confirm the use of encryption or not to start testing.
1. For External (Public) Test, you need to build with stable xcode, and the review process may takes one or two days.

## How to add custom pose in ROS/MoveIt - Wei
1. Add new pose parameters in `src/niryo_moveit/msg/NiryoMoveitJoints.msg` and `src/niryo_moveit/srv/MoverService.srv`. Check commit: [Add Approach & Retract Pose](https://gitlab.com/robots-do-what-you-see/pick_and_place/ROS/-/commit/3d2509341daece0923b713967a18fe80e4618125)
1. Handle the new pose in `src/niryo_moveit/scripts/mover.py`
1. Regenerate ROS Messgages in Unity
![](/media/generate_ros_msgs.webp)
1. Handle the new pose in Unity (e.g. [Assets/Scripts/TrajectoryPlanner.cs](https://gitlab.com/robots-do-what-you-see/pick_and_place/PickAndPlaceProject/-/commit/1b4f69f4fb276d68017d5b480ce49be743042a48))

## Try Hand Tracking - Wei

### Try [Manomotion Hand Tracking SDK](https://www.manomotion.com/)
This sdk uses TensorFlow Lite to handle the hand recognization.
With current version, it shows blackscreen/yellow jitter image on iOS. 
1. Import Universal Renderer Pipeline → create a pipeline Asset, add ARBackground
renderer feature, and set the renderer pipeline asset in Project settings - Graphics. Also materials need to be upgraded for URP (Edit- render pipeline- universal render pipeline - upgrade project materials...). Also see [How To Fix Black Screen Issue](https://www.youtube.com/watch?v=QcYlLI4KZOs).
1. Add 16 to depth to the render texture used to get the AR Background:
    ```csharp 
    // Assets/ManoMotion ARFoundation/Scripts/InputManagerArFoundation.cs
    protected override void InitializeInputParameters()
    {
        textureFormat = TextureFormat.RGBA32;

        frameTexture = new Texture2D(MinRezValue, MaxRezValue, textureFormat, false);
        pixelColors = new Color32[MaxRezValue * MinRezValue];
        // Fix black screen
        int depth = 16;
        inputRenderTexture = new RenderTexture(MinRezValue, MaxRezValue, depth);
        ...

    void ResizeInputRenderTexture(int width, int height)
    {
        if (inputRenderTexture != null)
        {
            inputRenderTexture.Release();
        }
        // Fix black screen
        int depth = 16;
        inputRenderTexture = new RenderTexture(width, height, depth);
    }
    ```
1. This works with Unity 2021 not Unity 2022.

### Try [Sony ToF Hand Tracking SDK](https://developer.sony.com/develop/tof-ar)
This sdk also uses TensorFlow Lite to handle the hand recognization.
For iphone 13, only front camera is Supported

## Azure Spatial Anchors - Camilo

### Add Initial Configuration

1. Create an Azure account, you can take a look at the following tutorial. [Azure Spatial Anchors](https://docs.microsoft.com/en-us/azure/spatial-anchors/quickstarts/get-started-unity-ios?tabs=azure-portal)
2. Clone the sample App Repo on your computer 
```git
git clone https://github.com/Azure/azure-spatial-anchors-samples.git
```
3. Open the unity project (pick and place project) and copy the AzureSpatialAnchor.Examples project folder from the repository you just cloned.
4. Import the ASA SDK from here [ASA SDK](https://dev.azure.com/aipmr/MixedReality-Unity-Packages/_artifacts/feed/Unity-packages) Import the following packages:
    com.microsoft.azure.spatial-anchors-sdk.core@<version_number>
    com.microsoft.azure.spatial-anchors-sdk.ios@<version_number>
    ** use the current version number. Version tested with 2.13.0
5. Import the files into the unity project using the following tutorial [Import Tarball Packages](https://docs.unity3d.com/Manual/upm-ui-tarball.html)
6. On the project window go to Assets\AzureSpatialAnchors.SDK\Resources and configure the *Account Key*, *Accound ID* and *Account Domain* with your Azure account. 


### Add Functionality

1. Open the Assets\AzureSpatialAnchors.Examples\Script\AzureSpatialAnchorsBasicDemoScript and add the following script:
```csharp
public async void ConfigureSession2()
        {
            currentAppState = AppState.DemoStepCreateSession;
            for (int i =0; i <2; i++)
            {
                AdvanceDemoAsync();
                await AdvanceDemoAsync();
                Debug.Log("Succesfully called the function");
            }
        }
        public async void CreateSpatialAnchor()
        {
            currentAppState = AppState.DemoStepBusy; 
            if (spawnedObject != null)
            {
                currentAppState = AppState.DemoStepSaveCloudAnchor;
            }
            else
            {
                currentAppState = AppState.DemoStepCreateLocalAnchor;
            }
            AdvanceDemoAsync();
            await SaveCurrentObjectAnchorToCloudAsync();
        }
        public async void ResolveAnchor()
        {
            currentAppState = AppState.DemoStepCreateSessionForQuery;
            ConfigureSession();
            currentAppState = AppState.DemoStepStartSessionForQuery;
            currentAppState = AppState.DemoStepBusy;
            await CloudManager.StartSessionAsync();
            currentAppState = AppState.DemoStepLookForAnchor;
            currentAppState = AppState.DemoStepLookingForAnchor;
            if (currentWatcher != null)
            {
                currentWatcher.Stop();
                currentWatcher = null;
            }
            currentWatcher = CreateWatcher();
            if (currentWatcher == null)
            {
                Debug.Log("Either cloudmanager or session is null, should not be here!");
                feedbackBox.text = "YIKES - couldn't create watcher!";
                currentAppState = AppState.DemoStepLookForAnchor;
            }
            AdvanceDemoAsync();


        }
        public async void Clear()
        {
            currentAppState = AppState.DemoStepBusy;
            CloudManager.StopSession();
            CleanupSpawnedObjects();
            await CloudManager.ResetSessionAsync();
        }
        public async void DeleteAnchors()
        {
            currentAppState = AppState.DemoStepBusy;
            await CloudManager.DeleteAnchorAsync(currentCloudAnchor);
            CleanupSpawnedObjects();
            currentAppState = AppState.DemoStepStopSessionForQuery;
            currentAppState = AppState.DemoStepBusy;
            CloudManager.StopSession();
            currentWatcher = null;
            currentAppState = AppState.DemoStepComplete;
            currentAppState = AppState.DemoStepBusy;
            currentCloudAnchor = null;
            CleanupSpawnedObjects();
            currentAppState = AppState.DemoStepCreateSession;
        }
        public async void Anchors2()
        {
            currentAppState = AppState.DemoStepBusy;
            if (spawnedObject != null)
            {
                currentAppState = AppState.DemoStepSaveCloudAnchor;
            }
            else
            {
                currentAppState = AppState.DemoStepCreateLocalAnchor;
            }
            AdvanceDemoAsync();
            await SaveCurrentObjectAnchorToCloudAsync();

        }
```
2. In the unity project folder open the Basic Demo Scene and in the Hierarchy View Select UXPArent -> Mobile UX.
3. Add 4 Buttons (One for each of the functions above)
4. Configure each button going in the inspector view and configure how is showed in the following picture.
![](/media/button_config.webp)
    Add each one of the functions above to the created buttons
    Configure the UI elements as you please (make sure they don't overlap each other)

### Configure the Scene

1. Open the BasicDemo Scene 
2. Create a new Robot Prefab
    Go to  Assets\AzureSpatialAnchors.Examples\Prefab\AzureSpatialAnchorsDemoObject
    Create a Prefab Variant 
    Add the RosAnchor Prefab as a Child of it
3. Go to the AzureSpatialAnchor Game Object 
![](/media/AzureSpatialAnchorGameObject.webp)
In the Anchor Object Prefab select the prefab you created.


### Build the Scene

1. Build Settings 
2. Select the Basic Demo Scene 
3. Build 



    
