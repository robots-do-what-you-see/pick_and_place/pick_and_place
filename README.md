# Pick And Place Demo

## About
This repo is based on [Unity Robotics Hub](https://github.com/Unity-Technologies/Unity-Robotics-Hub) with AR feature according to [ARとUnity-Robotics-Hubの連携](https://youtu.be/2CtzSHunKMo) and its [Slide](https://www.slideshare.net/UnityTechnologiesJapan002/unity-robotics-lt20219).

This repo is demo of trying to control robot in an intuitive way for the DDP class 2022 SS at RWTH Aachen University.

It contians two submodules: **PickAndPlaceProject** for Unity Projcet, and **ROS** for ROS intergration.

## Steps
1. Clone the repo & init submodule
    ```sh
    git clone --recurse-submodules git@gitlab.com:robots-do-what-you-see/pick_and_place/pick_and_place.git
    # if you forget to add --recurse-submodules:
    git submodule update --init 
    ```
1. Build docker image
    ```
    cd ROS
    ./build_image.sh
    ```
1. Launch Planner
    ```
    ./launch_planner.sh
    ```
1. Open PickAndPlaceProject with Unity Hub, move 
 
    `Library/PackageCache/com.unity.robotics.urdf-importer@90f353e435` 
 
    to `Packages/`

    and toggle

     `Packages/URDF Importer/Runtime/Unity.Robotics.URDFImporter.asmdef->Platforms/iOS` (and android when building for android)
1. Add Azure Spatial Anchors SDK core & ios (and android when build for android) package with NPM or download directly. Then add to Unity Project with Window/Package Manager/add package from tarball...
    ```
    npm pack com.microsoft.azure.spatial-anchors-sdk.core --registry https://pkgs.dev.azure.com/aipmr/MixedReality-Unity-Packages/_packaging/Unity-packages/npm/registry/
    npm pack com.microsoft.azure.spatial-anchors-sdk.ios --registry https://pkgs.dev.azure.com/aipmr/MixedReality-Unity-Packages/_packaging/Unity-packages/npm/registry/
    ```
1. Build. For iOS, signing a Team before building the xcode project.
1. Click `Place Robot` to put the robot in a proper place
1. Click `Save Anchor` to save the anchor to cloud (with)
1. Click `Load Anchor` to load pre-saved anchor and place the robot to the anchor
1. Click `Settings` to change ROS IP/Port, Connect on Start or Connect Manually, Show/Hide Table. Click `Save & Close` to close settings panel or Click `Settings` without saving.
1. Click `Next` or `Previous` to simulate pick and place process when connected to ROS Server.
